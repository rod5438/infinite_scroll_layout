//
//  SceneDelegate.h
//  InfiniteScrollLayout
//
//  Created by Jason Wu on 2020/3/18.
//  Copyright © 2020 Jason Wu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

