//
//  MyModelProvider.m
//  InfiniteScrollLayout
//
//  Created by Jason Wu on 2020/3/21.
//  Copyright © 2020 Jason Wu. All rights reserved.
//

#import "MyModelProvider.h"
#import <FMDB.h>

@interface NSArray (MyModel)

- (void)insertDB:(FMDatabase *)db;

@end

@implementation NSArray (MyModel)

- (void)insertDB:(FMDatabase *)db
{
    [self enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[MyModel class]]) {
            MyModel *model = (MyModel *)obj;
            if(![db executeUpdate:@"INSERT INTO my_models (id, title, image_url, type) VALUES (?,?,?,?)" withArgumentsInArray:@[@(model.modelIndex), model.title, model.imageUrl, @(model.typeInt)]]) {
                NSLog(@"Could not insert data: %@", [db lastErrorMessage]);
            }
        }
    }];
}

@end

@interface MyModelProvider ()

@property (readwrite) NSArray <MyModel *> *models;
@property (readonly) NSMutableArray <void (^)(void)> *getNextCompletion;
@property (readonly) FMDatabase *db;

@end

@implementation MyModelProvider

+ (instancetype)sharedInstance
{
    static MyModelProvider *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[MyModelProvider alloc] init];
    });
    return instance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSURL *appUrl = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
        NSString *dbPath = [[appUrl path] stringByAppendingPathComponent:@"MyDatabase.db"];
        FMDatabase* db = [FMDatabase databaseWithPath:dbPath];
        if (![db open]) {
            NSLog(@"Could not open db");
        }
        if(![db executeUpdate:@"CREATE TABLE IF NOT EXISTS my_models (id INTEGER PRIMARY KEY, title TEXT, image_url TEXT, type INTEGER)"]) {
            NSLog(@"Could not create table: %@", [db lastErrorMessage]);
        }
        _db = db;
        self.models = @[];
        _getNextCompletion = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)initModelsToModelIndex:(NSInteger)toModelIndex withCompletion:( void (^_Nonnull)(void))completion
{
    [self.getNextCompletion addObject:completion];
    if (self.getNextCompletion.count == 1) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSArray <MyModel *> *newModels = [self fetchModelFromIndex:0 andLength:toModelIndex + 1];
            self.models = newModels;
            if (self.getNextCompletion.count > 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.getNextCompletion enumerateObjectsUsingBlock:^(void (^ _Nonnull obj)(void), NSUInteger idx, BOOL * _Nonnull stop) {
                        obj();
                    }];
                    [self.getNextCompletion removeAllObjects];
                });
            }
        });
    }
}

- (void)fetchNextModelsWithCompletion:(void (^ _Nonnull )(void))completion
{
    [self.getNextCompletion addObject:completion];
    if (self.getNextCompletion.count == 1) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSArray <MyModel *> *newModels = [self fetchModelFromIndex:self.models.count andLength:30];
            self.models = [self.models arrayByAddingObjectsFromArray:newModels];
            if (self.getNextCompletion.count > 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.getNextCompletion enumerateObjectsUsingBlock:^(void (^ _Nonnull obj)(void), NSUInteger idx, BOOL * _Nonnull stop) {
                        obj();
                    }];
                    [self.getNextCompletion removeAllObjects];
                });
            }
        });
    }
}

- (NSArray <MyModel *> *)fetchModelFromIndex:(NSInteger)index andLength:(NSInteger)length
{
    NSMutableArray <MyModel *> *dbModels = [[NSMutableArray alloc] init];
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM my_models WHERE id >= %ld AND id < %ld", index, index + length];
    FMResultSet *s = [self.db executeQuery:sql];
    while ([s next]) {
        NSInteger index = [s intForColumn:@"id"];
        NSString *title = [s stringForColumn:@"title"];
        NSString *imageUrl = [s stringForColumn:@"image_url"];
        NSInteger typeInt = [s intForColumn:@"type"];
        [dbModels addObject:[[MyModel alloc] initWithType:typeInt == 0 ? @"titleImage" : @"titleFullImage" andTitl:title andImageUrl:imageUrl andIndex:index]];
    }
    if (dbModels.count == length) {
        return [dbModels copy];
    }
    // TODO: Should fetch from internet
    sleep(2); // take a long time because bad network conditions
    NSMutableArray <MyModel *> *fetchModels = [[NSMutableArray alloc] init];
    for (NSInteger currentIndex = index ; currentIndex < index + length ; currentIndex++) {
        NSUInteger find = [dbModels indexOfObjectPassingTest:^BOOL(MyModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            return obj.modelIndex == currentIndex;
        }];
        if (find == NSNotFound) {
            [fetchModels addObject:[self fetchModelFromIndex:currentIndex]];
        }
    }
    [fetchModels insertDB:self.db];
    return [[dbModels arrayByAddingObjectsFromArray:fetchModels] sortedArrayUsingComparator:^NSComparisonResult(MyModel *obj1, MyModel *obj2) {
        return obj1.modelIndex < obj2.modelIndex ? NSOrderedAscending : NSOrderedDescending;
    }];
}

- (MyModel *)fetchModelFromIndex:(NSInteger)index
{
    NSNumber *number = [NSNumber numberWithInteger:index];
    NSInteger pseudoRandom = (number.hash / 1000) % 2;
    return pseudoRandom == 0 ? [MyModel getTitleImageTypeWithIndex:index] : [MyModel getTitleFullImageTypeWithIndex:index];
    
}

@end
