//
//  MyCell.m
//  InfiniteScrollLayout
//
//  Created by Jason Wu on 2020/3/20.
//  Copyright © 2020 Jason Wu. All rights reserved.
//

#import "MyCell.h"
#import <SDWebImage.h>

@interface MyCell ()

@property (nonatomic) IBOutlet UIImageView *modelImageView;
@property (nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation MyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setImageUrl:(NSString *)imageUrl
{
    [self.modelImageView sd_setImageWithURL:[[NSURL alloc] initWithString:imageUrl]];
}

- (void)setTitle:(NSString *)title
{
    self.titleLabel.text = title;
}

@end
