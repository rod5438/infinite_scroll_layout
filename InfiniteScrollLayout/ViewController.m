//
//  ViewController.m
//  InfiniteScrollLayout
//
//  Created by Jason Wu on 2020/3/18.
//  Copyright © 2020 Jason Wu. All rights reserved.
//

#import "ViewController.h"
#import "MyModelProvider.h"
#import "MyCell.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@property IBOutlet UITableView *tableView;
@property IBOutlet MyModelProvider *provider;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.provider = [MyModelProvider sharedInstance];
    NSInteger modelIndex = [self lastModelIndex];
    [self.provider initModelsToModelIndex:modelIndex withCompletion:^() {
        [self.tableView reloadData];
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:modelIndex inSection:0];
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
    }];
}

- (NSInteger)lastModelIndex
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:@"currentSelectModelIndex"];
}

- (void)setLastModelIndex:(NSInteger)lastModelIndex
{
    [[NSUserDefaults standardUserDefaults] setInteger:lastModelIndex forKey:@"currentSelectModelIndex"];
}

#pragma - mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.provider.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MyCell *cell = [tableView dequeueReusableCellWithIdentifier:self.provider.models[indexPath.item].type];
    cell.imageUrl = self.provider.models[indexPath.item].imageUrl;
    cell.title = self.provider.models[indexPath.item].title;
    return cell;
}

#pragma - mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self setLastModelIndex:self.provider.models[indexPath.item].modelIndex];
    if (indexPath.item + 1 == self.provider.models.count) {
        [self.provider fetchNextModelsWithCompletion:^() {
            [self.tableView reloadData];
        }];
    }
}

@end

