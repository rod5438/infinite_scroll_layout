//
//  Model.h
//  InfiniteScrollLayout
//
//  Created by Jason Wu on 2020/3/20.
//  Copyright © 2020 Jason Wu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyModel : NSObject

- (instancetype)initWithType:(NSString *)type andTitl:(NSString *)title andImageUrl:(NSString *)imageUrl andIndex:(NSInteger)index;

@property (readonly) NSInteger modelIndex;
@property (readonly) NSString *type;
@property (readonly) NSInteger typeInt;
@property (readonly) NSString *title;
@property (readonly) NSString *imageUrl;

@end

@interface MyModel (test)

+ (MyModel *)getTitleImageTypeWithIndex:(NSInteger)index;
+ (MyModel *)getTitleFullImageTypeWithIndex:(NSInteger)index;

@end

NS_ASSUME_NONNULL_END
