//
//  MyCell.h
//  InfiniteScrollLayout
//
//  Created by Jason Wu on 2020/3/20.
//  Copyright © 2020 Jason Wu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyCell : UITableViewCell

@property (nonatomic) NSString *imageUrl;
@property (nonatomic) NSString *title;

@end

NS_ASSUME_NONNULL_END
