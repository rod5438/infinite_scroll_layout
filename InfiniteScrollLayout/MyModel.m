//
//  Model.m
//  InfiniteScrollLayout
//
//  Created by Jason Wu on 2020/3/20.
//  Copyright © 2020 Jason Wu. All rights reserved.
//

#import "MyModel.h"

@interface MyModel ()

@property (readwrite) NSInteger modelIndex;
@property (readwrite) NSString *type;
@property (readwrite) NSString *title;
@property (readwrite) NSString *imageUrl;

@end

@implementation MyModel

- (instancetype)initWithType:(NSString *)type andTitl:(NSString *)title andImageUrl:(NSString *)imageUrl andIndex:(NSInteger)index;
{
    self = [super init];
    if (self) {
        _modelIndex = index;
        _type = type;
        _title = title;
        _imageUrl = imageUrl;
    }
    return self;
}

- (NSInteger)typeInt
{
    if ([self.type isEqualToString:@"titleFullImage"]) {
        return 1;
    }
    else {
        return 0;
    }
}

@end

@implementation MyModel (test)

+ (MyModel *)getTitleImageTypeWithIndex:(NSInteger)index
{
    return [[MyModel alloc] initWithType:@"titleImage" andTitl:[NSString stringWithFormat:@"%4ld %@", (long)index, @"China panda offer requires further discussion...| Taiwan News"] andImageUrl:@"https://images.pexels.com/photos/1661535/pexels-photo-1661535.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" andIndex:index];
}

+ (MyModel *)getTitleFullImageTypeWithIndex:(NSInteger)index
{
    return [[MyModel alloc] initWithType:@"titleFullImage" andTitl:[NSString stringWithFormat:@"%4ld %@", (long)index, @"Fo Guang Shan was Breath Taking ! Take Me Back!"] andImageUrl:@"https://images.pexels.com/photos/3522880/pexels-photo-3522880.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" andIndex:index];
}

@end
