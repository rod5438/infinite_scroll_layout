//
//  MyModelProvider.h
//  InfiniteScrollLayout
//
//  Created by Jason Wu on 2020/3/21.
//  Copyright © 2020 Jason Wu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyModelProvider : NSObject

@property (readonly) NSArray <MyModel *> *models;

+ (instancetype)sharedInstance;
- (void)initModelsToModelIndex:(NSInteger)toModelIndex withCompletion:( void (^_Nonnull)(void))completion;
- (void)fetchNextModelsWithCompletion:( void (^_Nonnull)(void))completion;

@end

NS_ASSUME_NONNULL_END
